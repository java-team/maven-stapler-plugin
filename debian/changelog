maven-stapler-plugin (1.17-2) unstable; urgency=medium

  * Team upload.
  * Removed the -java-doc package (useless and fails to build with Java 8)
  * Standards-Version updated to 3.9.6 (no changes)
  * debian/copyright:
    - Fixed the short name for the BSD license
    - Updated the copyrights for the version 1.17
  * debian/watch: Replaced githubredir.debian.net

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 08 Sep 2015 12:49:06 +0200

maven-stapler-plugin (1.17-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Fixed a Javadoc error breaking the build with Java 8u20
  * Removed java7-compat.patch (fixed upstream)
  * Ignore the Maven dependency on animal-sniffer-maven-plugin
  * Removed the unused rules from debian/maven.rules

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 08 Aug 2014 23:50:55 +0200

maven-stapler-plugin (1.16-5) unstable; urgency=low

  * Team upload.
  * Use the maven-compiler-plugin 2.5.1 (Closes: #730886)
  * debian/control:
    - Build depend directly on libplexus-compiler-java
    - Changed the Homepage field to point to the Github project
    - Updated Standards-Version to 3.9.5 (no changes)
    - Removed the deprecated DM-Upload-Allowed field
    - Use canonical URLs for the Vcs-* fields
  * Marked the Java 7 compatibility patch as forwarded upstream
  * Switch to debhelper level 9

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 02 Dec 2013 17:29:57 +0100

maven-stapler-plugin (1.16-4) unstable; urgency=low

  * Fix FTBFS with openjdk-7 (LP: #888980):
    - d/patches/java7-compat.patch: Add profile to pickup tools.jar for
      OpenJDK7.
  * Bumped Standards-Version to 3.9.3:
    - d/copyright: Reference released version of DEP-5.

 -- James Page <james.page@ubuntu.com>  Wed, 20 Jun 2012 19:34:31 +0100

maven-stapler-plugin (1.16-3) unstable; urgency=low

  * d/control: Fixup missing runtime dependencies due to failure in 
    automatic detection using maven-debian-helper.

 -- James Page <james.page@ubuntu.com>  Wed, 08 Feb 2012 13:33:38 +0000

maven-stapler-plugin (1.16-2) unstable; urgency=low

  [James Page]
  * d/patches/stapler-compat.patch: Exclude Java 6 annotation processors from 
    build and jar files to avoid annotation processing conflict with 
    libstapler-java >= 1.169.

  [tony mancill]
  * Set DMUA flag.

 -- James Page <james.page@ubuntu.com>  Tue, 24 Jan 2012 14:17:15 +0000

maven-stapler-plugin (1.16-1) unstable; urgency=low

  * Initial Debian release (Closes: #631961)
  * New upstream release
  * d/copyright: Updated with new copyright/licensing information.

 -- James Page <james.page@ubuntu.com>  Wed, 26 Oct 2011 17:11:45 +0100

maven-stapler-plugin (1.15-0ubuntu1) oneiric; urgency=low

  * Initial release 

 -- James Page <james.page@ubuntu.com>  Tue, 28 Jun 2011 17:35:14 +0100
